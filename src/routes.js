const routes = {
  home: {
    pattern: "/",
    make: () => "/",
  },
  login: {
    pattern: "/login",
    make: () => "/login",
  },
  register: {
    pattern: "/register",
    make: () => "/register",
  },
  images: {
    pattern: "/images",
    make: () => "/images",
  },
  videos: {
    pattern: "/videos",
    make: () => "/videos",
  },
};

export default routes;
