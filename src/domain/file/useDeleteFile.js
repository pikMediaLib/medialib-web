import { message } from "antd";
import api from "services/api";

export default function useDeleteFile(onDelete) {
  const deleteFile = async (id) => {
    const response = await api.deleteFile({ id });

    if (response.status) {
      message.success("Deleted successfully");
      onDelete();
    } else {
      message.error("Error while deleting file");
    }
  };

  return deleteFile;
}
