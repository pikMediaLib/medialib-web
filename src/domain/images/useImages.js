import { useEffect, useState } from "react";
import api from "services/api";

export default function useImages() {
  const [images, setImages] = useState();

  const fetchImages = async () => {
    const response = await api.photos();

    if (response.status) {
      setImages(response.data);
    }
  };

  useEffect(() => fetchImages(), []);

  return [images, fetchImages];
}
