import { useEffect, useState } from "react";
import api from "services/api";

export default function useVideos() {
  const [videos, setVideos] = useState();

  const fetchVidoes = async () => {
    const response = await api.videos();

    if (response.status) {
      setVideos(response.data);
    }
  };

  useEffect(() => fetchVidoes(), []);

  return [videos, fetchVidoes];
}
