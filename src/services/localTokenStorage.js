export default function mkLocalTokenStorage({ onTokenChanged, tokenKey = "token" }) {
  let dispose = () => {};

  if (onTokenChanged) {
    const handler = (ev) => {
      if ([tokenKey].includes(ev.key)) {
        onTokenChanged(getInitialToken());
      }
    };

    window.addEventListener("storage", handler);

    dispose = () => window.removeEventListener("storage", handler);
  }

  function getInitialToken() {
    const token = localStorage.getItem(tokenKey);

    if (token !== null) {
      return token;
    }
  }

  function saveToken(token) {
    if (token) {
      console.log("save token");
      localStorage.setItem(tokenKey, token);
    } else {
      localStorage.removeItem(tokenKey);
    }
  }

  return { getInitialToken, saveToken, dispose };
}
