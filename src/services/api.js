import axios from "axios";

const apiClient = axios.create({
  baseURL: "http://localhost:8090/api",
});

apiClient.interceptors.request.use((request) => {
  const apiToken = localStorage.getItem("token");

  if (apiToken) {
    request.headers["X-API-TOKEN"] = apiToken;
  }

  return request;
});

const api = {
  photos: () => apiClient.get(`/all?type=image`),
  videos: () => apiClient.get(`/all?type=video`),
  deleteFile: ({ id }) => apiClient.delete(`/file/${id}`),
  editFile: ({ id, displayName }) => apiClient.post(`/file/${id}`, { displayName }),
  uploadLink: ({ id }) => apiClient.get(`upload?fileId=${id}`),
};

export default api;
