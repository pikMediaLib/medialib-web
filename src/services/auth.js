import axios from "axios";

const oauthBase = axios.create({
  baseURL: "http://localhost:8090/oauth/",
});

const oauth = {
  login: ({ email, password }) => oauthBase.post("/login", { email, password }),
  register: ({ name, surname, email, password }) => oauthBase.post("/signup", { name, surname, email, password }),
};

export default oauth;
