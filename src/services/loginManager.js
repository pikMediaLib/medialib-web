import { action, computed, decorate, observable, runInAction } from "mobx";
import oauth from "./auth";
import mkLocalTokenStorage from "./localTokenStorage";

class LoginManager {
  authAxios;
  tokenStorage;

  internalToken;

  get token() {
    return this.internalToken;
  }

  set token(token) {
    this.internalToken = token;
    this.tokenStorage.saveToken(this.token);
  }

  get isSigned() {
    return !!this.token;
  }

  constructor({ tokenStorageFactory }) {
    this.tokenStorage = tokenStorageFactory((token) => {
      runInAction(() => {
        this.internalToken = token;
      });
    });

    runInAction(() => {
      this.internalToken = this.tokenStorage.getInitialToken();
    });
  }

  async signIn(data) {
    try {
      const result = await oauth.login({ ...data });

      if (result.status) {
        const token = result.data;

        runInAction(() => {
          this.token = token;
        });

        return "success";
      }
    } catch (e) {
      const isAxiosError = (e) => {
        return "response" in e;
      };

      if (isAxiosError(e) && e.response.status === 400) {
        this.signOut();

        return "failure";
      }

      return "networkError";
    }
  }

  signOut() {
    this.token = undefined;
  }
}

decorate(LoginManager, {
  internalToken: observable,
  token: computed,
  isSigned: computed,
  signOut: action.bound,
});

export const loginManager = new LoginManager({
  tokenStorageFactory: (onTokenChanged) => mkLocalTokenStorage({ onTokenChanged }),
});

export function signInWithEmailAndPassword(email, password) {
  return loginManager.signIn({
    email,
    password,
  });
}

export function signOut() {
  return loginManager.signOut();
}

export default loginManager;
