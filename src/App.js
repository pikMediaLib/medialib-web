import "antd/dist/antd.css";
import AuthGuard from "components/auth/AuthGuard";
import AuthRedirection from "components/auth/AuthRedirection";
import AppLayout from "components/common/AppLayout";
import AuthLayout from "components/common/AuthLayout";
import ImagesPage from "pages/images";
import LoginPage from "pages/login";
import RegisterPage from "pages/register";
import VideosPage from "pages/videos";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import theme from "styles/theme";
import HomePage from "./pages/index";
import routes from "./routes";

const AuthenticatedRoutes = () => (
  <AppLayout>
    <Switch>
      <Route exact path={routes.home.pattern} component={HomePage} />
      <Route exact path={routes.images.pattern} component={ImagesPage} />
      <Route exact path={routes.videos.pattern} component={VideosPage} />
    </Switch>
  </AppLayout>
);

const AuthRoutes = () => (
  <AuthLayout>
    <Switch>
      <Route exact path={[routes.login.pattern]} component={LoginPage} />
      <Route exact path={[routes.register.pattern]} component={RegisterPage} />
    </Switch>
  </AuthLayout>
);

function App() {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Switch>
          <Route path={[routes.login.pattern, routes.register.pattern]}>
            <AuthRedirection component={AuthRoutes} />
          </Route>
          <Route path={[routes.home.pattern, routes.images.pattern, routes.videos.pattern]}>
            <AuthGuard component={AuthenticatedRoutes} />
          </Route>
        </Switch>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
