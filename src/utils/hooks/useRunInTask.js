import { useCallback, useState } from "react";

export default function useRunInTask() {
  const [isRunning, setIsRunning] = useState(false);
  const runInTask = useCallback(async (block) => {
    setIsRunning(true);
    try {
      return await block();
    } finally {
      setIsRunning(false);
    }
  }, []);
  return [isRunning, runInTask];
}
