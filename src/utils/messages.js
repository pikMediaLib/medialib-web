const { message } = require("antd");

export const displayCommonErrorMessage = () => message.error("Something went wrong. Please try again later");
