import FullScreenLoader from "components/common/FullScreenLoader";
import FileSearch from "components/file/FileSearch";
import UploadVideoButton from "components/videos/UploadVideoButton";
import VideoTile from "components/videos/VideoTile";
import useVideos from "domain/videos/useVideos";
import React, { useState } from "react";
import "react-slideshow-image/dist/styles.css";
import styled from "styled-components";

const VideosPage = () => {
  const [videos, fetchVideos] = useVideos();
  const [searchText, setSearchText] = useState("");

  if (videos === undefined) {
    return <FullScreenLoader />;
  }

  return (
    <Root>
      <Header>
        <FileSearch value={searchText} onSearch={setSearchText} />
        <UploadVideoButton onUpload={fetchVideos} />
      </Header>
      <Content>
        {videos
          .filter((img) => img.displayName.includes(searchText ?? ""))
          .map((video, i) => (
            <VideoTile key={video.fileId} video={video} onDelete={fetchVideos} />
          ))}
      </Content>
    </Root>
  );
};

const Root = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${({ theme }) => theme.spacings.medium};

  width: 100%;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;

  flex: 1 0 0;
`;

const Content = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: ${({ theme }) => theme.spacings.medium};
`;

export default VideosPage;
