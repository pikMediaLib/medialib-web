import FullScreenLoader from "components/common/FullScreenLoader";
import FileSearch from "components/file/FileSearch";
import ImageTile from "components/images/ImageTile";
import UploadImageButton from "components/images/UploadImageButton";
import useImages from "domain/images/useImages";
import React, { useState } from "react";
import "react-slideshow-image/dist/styles.css";
import styled from "styled-components";

const ImagesPage = () => {
  const [images, fetchImages] = useImages();
  const [searchText, setSearchText] = useState("");

  if (images === undefined) {
    return <FullScreenLoader />;
  }

  return (
    <Root>
      <Header>
        <FileSearch value={searchText} onSearch={setSearchText} />
        <UploadImageButton onUpload={fetchImages} />
      </Header>
      <Content>
        {images
          .filter((img) => (img.displayName ? img.displayName.includes(searchText ?? "") : true))
          .map((img, i) => (
            <ImageTile key={img.fileId} img={img} onDelete={fetchImages} />
          ))}
      </Content>
    </Root>
  );
};

const Root = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${({ theme }) => theme.spacings.medium};

  width: 100%;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;

  flex: 1 0 0;
`;

const Content = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: ${({ theme }) => theme.spacings.medium};
`;

export default ImagesPage;
