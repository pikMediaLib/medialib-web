import { Button } from "antd";

const DeleteFileButton = ({ isLoading, onClick }) => {
  return (
    <Button danger type="primary" onClick={onClick} loading={isLoading}>
      Delete file
    </Button>
  );
};

export default DeleteFileButton;
