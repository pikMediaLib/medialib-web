import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import { message, Upload } from "antd";
import React, { useState } from "react";

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file, extArr) {
  console.log(file.type);

  const isCorrectExt = extArr.some((v) => v === file.type);

  if (!isCorrectExt) {
    message.error("You can only upload JPG/PNG file!");
  }

  return isCorrectExt;
}

const dummyRequest = ({ file, onSuccess }) => {
  setTimeout(() => {
    onSuccess("ok");
  }, 0);
};

const FileUpload = ({ value, type, extArr, onChange }) => {
  const [isLoading, setIsLoadig] = useState(false);

  const handleChange = (info) => {
    if (info.file.status === "uploading") {
      setIsLoadig(true);
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.

      const ext = info.file.name.split(".").pop();

      getBase64(info.file.originFileObj, (imageUrl) => {
        setIsLoadig(false);
        onChange({ imageUrl, ext });
      });
    }
  };

  const uploadButton = (
    <div>
      {isLoading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  return (
    <Upload
      name="avatar"
      listType="picture-card"
      className="avatar-uploader"
      showUploadList={false}
      beforeUpload={(file) => beforeUpload(file, extArr)}
      onChange={handleChange}
      customRequest={dummyRequest}
    >
      {value ? <Sample src={value.imageUrl} type={type} /> : uploadButton}
    </Upload>
  );
};

const Sample = ({ src, type }) => {
  if (type === "image") {
    return <img src={src} alt="avatar" style={{ height: "100%" }} />;
  }

  return (
    <video width="100%" controls>
      <source src={src} type="video/mp4" />
    </video>
  );
};

export default FileUpload;
