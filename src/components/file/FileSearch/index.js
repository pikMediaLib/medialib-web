import { Input } from "antd";

const FileSearch = ({ value, onSearch }) => {
  return (
    <Input.Search
      value={value}
      placeholder="search by name"
      allowClear
      onChange={(v) => onSearch(v.target.value)}
      style={{ width: 200 }}
    />
  );
};

export default FileSearch;
