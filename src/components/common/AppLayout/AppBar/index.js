import React from "react";
import { Link } from "react-router-dom";
import routes from "routes";
import loginManager from "services/loginManager";
import styled from "styled-components";

const AppBar = () => {
  return (
    <Root>
      <Left>
        <Pane>
          <DefaultLink to={routes.images.make()}>Images</DefaultLink>
        </Pane>
        <Pane>
          <DefaultLink to={routes.videos.make()}>Videos</DefaultLink>
        </Pane>
      </Left>
      <Pane isAction onClick={loginManager.signOut}>
        Logout
      </Pane>
    </Root>
  );
};

const Root = styled.div`
  display: flex;
  justify-content: space-between;

  padding: ${({ theme }) => theme.spacings.medium};

  background-color: ${({ theme }) => theme.colors.secondary};
`;

const Left = styled.div`
  display: flex;
`;

const Pane = styled.div`
  color: ${({ theme }) => theme.colors.white};

  margin-left: ${({ theme }) => theme.spacings.medium};

  cursor: ${({ isAction }) => (isAction ? "pointer" : "default")};
`;

const DefaultLink = styled(Link)`
  color: inherit;

  &:hover {
    color: inherit;
  }
`;

export default AppBar;
