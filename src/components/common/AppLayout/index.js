import React from "react";
import styled from "styled-components";
import AppBar from "./AppBar";

const AppLayout = ({ children }) => {
  return (
    <Root>
      <AppBar />
      <Content>{children}</Content>
    </Root>
  );
};

const Root = styled.div`
  min-width: 100vw;
  min-height: 100vh;
`;

const Content = styled.div`
  padding: ${({ theme }) => theme.spacings.medium};
`;

export default AppLayout;
