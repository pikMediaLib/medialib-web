import { Spin } from "antd";
import React from "react";
import styled from "styled-components";

const FullScreenLoader = () => (
  <Root>
    <Spin size="large" />
  </Root>
);

const Root = styled.div`
  width: 100%;
  height: 90vh;

  display: flex;
  align-items: center;
  justify-content: center;

  background: white;
`;

export default FullScreenLoader;
