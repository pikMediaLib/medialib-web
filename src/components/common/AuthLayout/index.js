import React from "react";
import styled from "styled-components";

const AuthLayout = ({ children }) => {
  return <Root>{children}</Root>;
};

const Root = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  min-height: 100vh;
  min-width: 100vw;
`;

export default AuthLayout;
