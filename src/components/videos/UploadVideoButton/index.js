import { Button, Form, Input, message, Modal } from "antd";
import { useForm } from "antd/lib/form/Form";
import FileUpload from "components/file/FileUpload";
import React, { useEffect, useState } from "react";
import api from "services/api";
import useRunInTask from "utils/hooks/useRunInTask";
import { v4 as uuidv4 } from "uuid";

const nameField = "name";
const videoField = "video";

const UploadVideoButton = ({ onUpload }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [form] = useForm();
  const [isLoading, runInTask] = useRunInTask();

  const onSubmit = async (values) => {
    const isUploaded = await runInTask(async () => {
      const displayName = values[nameField];
      const extension = values[videoField].ext;
      const imageUrl = values[videoField].imageUrl;

      const id = `${uuidv4()}.${extension}`;
      const photoBlob = await fetch(imageUrl).then((res) => res.blob());

      const uploadLinkResponse = await api.uploadLink({ id });

      if (uploadLinkResponse.status) {
        const url = uploadLinkResponse.data.url;
        const headers = { "X-AUTH-TOKEN": uploadLinkResponse.data["X-AUTH-TOKEN"] };

        const uploadResponse = await fetch(url, {
          method: "PUT",
          headers: headers,
          body: photoBlob,
        });

        if (uploadResponse.status) {
          const editNameResponse = await api.editFile({ id, displayName });

          if (editNameResponse.status) {
            return true;
          }
        }
      }
      return false;
    });

    if (isUploaded) {
      message.success("File uploaded succesfully");
      onUpload?.();
      setIsOpen(false);
    } else {
      message.error("Something went wrong. Try again later");
    }
  };

  useEffect(() => {
    form.resetFields();
  }, [form, isOpen]);

  return (
    <>
      <Button type="primary" onClick={() => setIsOpen(true)}>
        Upload
      </Button>
      <Modal
        width="800px"
        visible={isOpen}
        onCancel={() => setIsOpen(false)}
        okText="Upload"
        title="Upload video"
        onOk={form.submit}
        confirmLoading={isLoading}
      >
        <Form name="upload" validateTrigger="onBlur" onFinish={onSubmit} form={form}>
          <Form.Item name={nameField} required={true}>
            <Input placeholder="name" size="large" />
          </Form.Item>
          <Form.Item name={videoField} required={true}>
            <FileUpload type="video" extArr={["video/mp4"]} />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default UploadVideoButton;
