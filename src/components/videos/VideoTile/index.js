import VideoModal from "components/videos/VideoModal";
import { useState } from "react";
import styled from "styled-components";

const VideoTile = ({ video, onDelete }) => {
  const [isOpen, setIsOpen] = useState();

  return (
    <>
      <Root onClick={() => setIsOpen(true)}>
        <Top>
          <video width="100%" controls>
            <source src={video.url} type="video/mp4" />
          </video>
        </Top>
        <Bottom>
          <Title>{video.displayName}</Title>
        </Bottom>
      </Root>
      <VideoModal isOpen={isOpen} onClose={() => setIsOpen(false)} video={video} onDelete={onDelete} />
    </>
  );
};

const Root = styled.div`
  display: flex;
  flex-direction: column;

  width: 360px;
  height: 240px;

  padding: ${({ theme }) => theme.spacings.medium};

  background-color: ${({ theme }) => theme.colors.white};
  border-radius: 8px;
  cursor: pointer;

  transition: all 0.15s ease-in-out;

  &:hover {
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  }
`;

const Top = styled.div`
  display: flex;
  justify-content: center;

  height: 70%;
`;

const Bottom = styled.div`
  display: flex;
  flex-direction: column;

  justify-content: center;

  height: 30%;
`;

const Title = styled.h3`
  margin: 0;
`;

export default VideoTile;
