import { message, Modal } from "antd";
import DeleteFileButton from "components/file/DeleteFileButton";
import React from "react";
import "react-slideshow-image/dist/styles.css";
import api from "services/api";
import styled from "styled-components";
import useRunInTask from "utils/hooks/useRunInTask";

const VideoModal = ({ isOpen, onClose, video, onDelete }) => {
  const [isLoading, runInTask] = useRunInTask();

  const deleteFile = async (id) => {
    runInTask(async () => {
      const response = await api.deleteFile({ id });

      if (response.status) {
        message.success("Deleted successfully");
        onDelete();
      } else {
        message.error("Error while deleting file");
      }
    });
  };

  return (
    <Modal width="800px" visible={isOpen} onCancel={onClose} footer={null}>
      <>
        <Header>{video.displayName}</Header>
        <video width="100%" controls>
          <source src={video.url} type="video/mp4" />
        </video>
        <Description>
          <Line>
            <Label>Size:</Label>
            <Content>{Math.round(video.size / 1024)} KB</Content>
          </Line>
        </Description>
        <DeleteFileButton onClick={() => deleteFile(video.fileId)} isLoading={isLoading} />
      </>
    </Modal>
  );
};

const Header = styled.h2``;

const Description = styled.div`
  margin: ${({ theme }) => theme.spacings.medium} 0;
`;

const Line = styled.div`
  display: flex;
  gap: 1rem;
`;

const Label = styled.p`
  color: ${({ theme }) => theme.colors.gray500};
`;

const Content = styled.p``;

export default VideoModal;
