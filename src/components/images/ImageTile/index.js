import ImageModal from "components/images/ImageModal";
import { useState } from "react";
import styled from "styled-components";

const ImageTile = ({ img, onDelete }) => {
  const [isOpen, setIsOpen] = useState();

  return (
    <>
      <Root onClick={() => setIsOpen(true)}>
        <Top>
          <Image src={img.url} alt="icon" />
        </Top>
        <Bottom>
          <Title>{img.displayName}</Title>
        </Bottom>
      </Root>
      <ImageModal isOpen={isOpen} onClose={() => setIsOpen(false)} img={img} onDelete={onDelete} />
    </>
  );
};

const Root = styled.div`
  display: flex;
  flex-direction: column;

  width: 360px;
  height: 240px;

  padding: ${({ theme }) => theme.spacings.medium};

  background-color: ${({ theme }) => theme.colors.white};
  border-radius: 8px;
  cursor: pointer;

  transition: all 0.15s ease-in-out;

  &:hover {
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  }
`;

const Top = styled.div`
  display: flex;
  justify-content: center;

  height: 70%;
`;

const Image = styled.img`
  height: 100%;
  width: 100%;
  object-fit: cover;

  border-radius: 4 px;
`;

const Bottom = styled.div`
  display: flex;
  flex-direction: column;

  justify-content: center;

  height: 30%;
`;

const Title = styled.h3`
  margin: 0;
`;

export default ImageTile;
