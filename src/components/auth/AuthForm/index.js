import { Typography } from "antd";
import React from "react";
import styled from "styled-components";

const AuthForm = ({ title, children }) => {
  return (
    <Root>
      <Header>
        <Title strong>{title}</Title>
      </Header>
      <Content>{children}</Content>
    </Root>
  );
};

const Root = styled.div`
  display: flex;
  flex-direction: column;

  width: 360px;

  padding: 0;

  background-color: ${({ theme }) => theme.colors.white};
  border-radius: 8px;
`;

const Header = styled.div`
  display: flex;
  align-items: center;

  padding: ${({ theme }) => theme.spacings.medium};

  background-color: ${({ theme }) => theme.colors.secondary};
  border-radius: 8px 8px 0 0;
`;

const Title = styled(Typography.Text)`
  color: ${({ theme }) => theme.colors.white};
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  padding: ${({ theme }) => theme.spacings.large};
`;

export default AuthForm;
