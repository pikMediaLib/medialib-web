import { toJS } from "mobx";
import { useObserver } from "mobx-react-lite";
import React from "react";
import { Redirect } from "react-router-dom";
import routes from "routes";
import loginManager from "services/loginManager";

const AuthGuard = ({ component: Component }) => {
  const isSignedIn = useObserver(() => loginManager.isSigned);

  console.log(toJS(isSignedIn));

  if (isSignedIn) {
    return <Component />;
  }

  return <Redirect to={routes.login.make()} />;
};

export default AuthGuard;
