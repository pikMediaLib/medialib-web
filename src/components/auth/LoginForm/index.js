import { Button, Form, Input, Typography } from "antd";
import { useForm } from "antd/lib/form/Form";
import AuthForm from "components/auth/AuthForm";
import React, { useCallback } from "react";
import { Link } from "react-router-dom";
import routes from "routes";
import { signInWithEmailAndPassword } from "services/loginManager";
import styled from "styled-components";
import useRunInTask from "utils/hooks/useRunInTask";

const emailField = "email";
const passwordField = "password";

const LoginForm = () => {
  const [form] = useForm();
  const [isLoading, runInTask] = useRunInTask();

  const onSubmit = useCallback(
    async (values) => {
      const result = await runInTask(() => signInWithEmailAndPassword(values[emailField], values[passwordField]));

      switch (result) {
        case "failure":
          form.setFields([{ name: passwordField, errors: ["Wrong e-mail or password."] }]);
          break;
        case "networkError":
          form.setFields([{ name: passwordField, errors: ["Network connection error. Try again later."] }]);
          break;
        default:
          break;
      }
    },
    [form, runInTask]
  );

  return (
    <AuthForm title="Login">
      <FullWidthForm name="login" validateTrigger="onBlur" onFinish={onSubmit} form={form}>
        <Form.Item name={emailField} required={true} rules={[{ required: true, type: "email" }]}>
          <Input placeholder="email" size="large" />
        </Form.Item>
        <Form.Item name={passwordField} required={true} rules={[{ required: true }]}>
          <Input name={passwordField} placeholder="password" type="password" size="large" />
        </Form.Item>
      </FullWidthForm>
      <Button type="primary" loading={isLoading} onClick={form.submit}>
        Login
      </Button>
      <Extra>
        <Typography.Text>No account?</Typography.Text>
        <Link to={routes.register.make()}>&nbsp;Create one</Link>
      </Extra>
    </AuthForm>
  );
};

const FullWidthForm = styled(Form)`
  width: 100%;
`;

const Extra = styled.div`
  margin-top: ${({ theme }) => theme.spacings.medium};
`;

export default LoginForm;
