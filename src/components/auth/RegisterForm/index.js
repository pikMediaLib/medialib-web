import { Button, Form, Input, message, Typography } from "antd";
import { useForm } from "antd/lib/form/Form";
import AuthForm from "components/auth/AuthForm";
import React from "react";
import { Link, useHistory } from "react-router-dom";
import routes from "routes";
import oauth from "services/auth";
import styled from "styled-components";
import useRunInTask from "utils/hooks/useRunInTask";
import { displayCommonErrorMessage } from "utils/messages";

const nameField = "name";
const surnameField = "surname";
const emailField = "email";
const passwordField = "password";

const RegisterForm = () => {
  const [form] = useForm();
  const [isLoading, runInTask] = useRunInTask();
  const { push } = useHistory();

  const register = async (values) => {
    const name = values[nameField];
    const surname = values[surnameField];
    const email = values[emailField];
    const password = values[passwordField];

    const result = await runInTask(() => oauth.register({ name, surname, email, password }));

    if (result.status) {
      message.success("Successfully registered");
      push(routes.login.make());
    } else {
      displayCommonErrorMessage();
    }
  };

  return (
    <AuthForm title="Register">
      <FullWidthForm form={form} name="login" validateTrigger="onBlur" onFinish={register}>
        <Form.Item name={nameField}>
          <Input placeholder="name" type="text" size="large" />
        </Form.Item>
        <Form.Item name={surnameField}>
          <Input placeholder="surname" type="text" size="large" />
        </Form.Item>
        <Form.Item name={emailField}>
          <Input placeholder="email" type="email" size="large" />
        </Form.Item>
        <Form.Item name={passwordField}>
          <Input name={passwordField} placeholder="password" type="password" size="large" />
        </Form.Item>
      </FullWidthForm>
      <Button type="primary" onClick={form.submit} loading={isLoading}>
        Create an account
      </Button>
      <Extra>
        <Typography.Text>Already have an account?</Typography.Text>
        <Link to={routes.login.make()}>&nbsp;Log in</Link>
      </Extra>
    </AuthForm>
  );
};

const FullWidthForm = styled(Form)`
  width: 100%;
`;

const Extra = styled.div`
  margin-top: ${({ theme }) => theme.spacings.medium};
`;

export default RegisterForm;
