import { useObserver } from "mobx-react-lite";
import React from "react";
import { Redirect } from "react-router-dom";
import routes from "routes";
import loginManager from "services/loginManager";

const NoAuthRedirection = ({ component: Component }) => {
  const isNotSignedIn = useObserver(() => !loginManager.isSigned);

  if (isNotSignedIn) {
    return <Component />;
  }

  return <Redirect to={routes.home.make()} />;
};

export default NoAuthRedirection;
