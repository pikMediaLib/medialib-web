import { render, screen } from "@testing-library/react";
import AppTest from "components/test";

test("renders learn react link", () => {
  render(<AppTest />);
  const linkElement = screen.getByText(/App test/i);
  expect(linkElement).toBeInTheDocument();
});
