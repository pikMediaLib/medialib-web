const theme = {
  colors: {
    primary: "#1890ff",
    secondary: "#fb8500",

    white: "#ffffff",
    gray500: "#757575",
  },
  spacings: {
    xsmall: "0.25rem", // 4
    small: "0.5rem", // 8
    medium: "1rem", // 16
    large: "1.5rem", // 24
    xlarge: "2rem", // 32
    xxlarge: "2.5rem", // 40
    xxxlarge: "3rem", // 48
  },
};

export default theme;
